## How to use
This Deck should be only used as an extension for the lecture. It contains most important definitions and theorems from both the lectures and the exercises. Solutions to those exercises are not included for obvious reasons.

## Definitions
Those should be learned by heart.

## Theorems
The Theorems themselves should be learned by heart, for the proofs it may be helpful to the understanding to learn them as well. Since the proofs can be quite long, they have been sliced into smaller (not quite bite-sized) pieces. It is your choice to just read them out loud when ever they come up, or actually try to keep all of it in your head. I tend to prefer the latter, since it helps me understand the proof thoroughly. But no pressure from my side.

## Quiz-Cards
![Screenshot](readmePics/question_answer.png)
In this deck, I decided to try out something Anki is not meant to do: Quiz cards. It's simple: Multiple choice questions with answers in a pseudo-randomized order. They are not checked automatically, but when you click on "view solution", the colors indicate weather a given statement is a correct answer to the question or not. There can be all right and all wrong cards. It's just more fun this way. You choose weather it went well. No automation what-so-ever. However: beware. There might be some bug i didn't notice and the answers could be scrambled. If you don't agree with my card, look it up so you don't learn mere rubbish. It would be nice if you sent every error you encounter to fredrik.konrad@rwth-aachen.de.

## Quiz-Decks
Another new addition are Quiz-Decks. As many problems and classes are just mere blobs of symbols (like $`\Sigma_k-\text{SAT}`$ and $`\textbf{BPP}`$), that I found it preferable to use the quiz cards in order to practice to tell them apart and don't mix theorems like $`\textbf{AL} = \textbf P`$ up with something that is wrong like $`\textbf{NL} = \textbf P`$. That happened to me an awful lot and I hope this helps me, and you :D

## What is still left to do
Since I did this primarily for myself, I left out things that I didn't need ASAP. As I write the exam tomorrow, there is some stuff that got left behind. I cannot promise that I will find the time to complete everything. If you are in another semester and want to update this deck, feel free to contact me. Here is what's still undone:

- Interactive Proof Systems and $`\textbf{IP} = \textbf{PSPACE}`$

- All the quiz decks, actually. They seemed like a good Idea, but i didn't even start them yet, because they would be very time consuming

- Some images. If I flagged a card blue, that's where I wanted to add one at some point






